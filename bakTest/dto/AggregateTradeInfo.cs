﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bakTest.dto
{
    public class AggregateTradeInfoResVo
    {
        public double timeStamp;

        public List<AggregateTradeInfo> list;
    }

    public class AggregateTradeInfo
    {
        public string symbol;

        public string price;

        public string quantity;
    }
}
