﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;
using bakTest.dto;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;

namespace bakTestDLL
{
    public class BakTest
    {
        // Mongo DB를 위한 Connection String
        string connString = "mongodb://1.227.103.10:37017";

        Dictionary<string, List<BsonDocument>> collection;

        MongoDatabase db;

        List<string> symbolList;

        public BakTest(List<string> symbolList)
        {
            this.symbolList = symbolList;

            // MongoClient 클라이언트 객체 생성
            var cli = new MongoClient(this.connString);

            // testdb 라는 데이타베이스 가져오기
            // 만약 해당 DB 없으면 Collection 생성시 새로 생성함
            this.db = cli.GetServer().GetDatabase("EXCHANGE");

            this.collection = new Dictionary<string, List<BsonDocument>>();

            symbolList.ForEach(s =>
            {
                this.collection.Add(s, this.db.GetCollection(s).FindAll().ToList());
            });
        }

        public AggregateTradeInfoResVo getData(double timeStamp)
        {
            var list = new List<AggregateTradeInfo>();

            this.symbolList.ForEach(s =>
            {
                this.collection[s].ForEach(row =>
                {
                    if (timeStamp == row["T"])
                    {
                        //Console.WriteLine(row);

                        var a = new AggregateTradeInfo();

                        a.symbol = s;
                        a.price = row["p"].ToString();
                        a.quantity = row["q"].ToString();

                        list.Add(a);
                    }
                });
            });

            if (list.Count == 0)
            {
                var newList = new List<AggregateTradeInfo>();

                var newTimeStamp = timeStamp + 1;
                var breakFlag = false;
                while (true)
                {
                    this.symbolList.ForEach(s =>
                    {
                        this.collection[s].ForEach(row =>
                        {
                            if (newTimeStamp == row["T"])
                            {
                                breakFlag = true;
                            }
                        });
                    });
                    if (breakFlag) break;
                    newTimeStamp++;
                };

                this.symbolList.ForEach(s =>
                {
                    this.collection[s].ForEach(row =>
                    {
                        if (newTimeStamp == row["T"])
                        {
                            var a = new AggregateTradeInfo();

                            a.symbol = s;
                            a.price = row["p"].ToString();
                            a.quantity = row["q"].ToString();

                            newList.Add(a);
                        }
                    });
                });

                var res = new AggregateTradeInfoResVo();

                res.timeStamp = newTimeStamp;
                res.list = newList;

                return res;
            }
            else
            {
                var res = new AggregateTradeInfoResVo();

                res.timeStamp = timeStamp;
                res.list = list;

                return res;
            }
        }
    }
}
