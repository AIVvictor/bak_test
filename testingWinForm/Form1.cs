﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using bakTest.dto;
using bakTestDLL;

namespace testingWinForm
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            timer1.Interval = 1000;
            timer1.Start();
        }

        public double timeStamp = 1577979000439;

        private void timer1_Tick(object sender, EventArgs e)
        {
            onTick(this.timeStamp);
        }

        private void onTick(double timeStamp)
        {
            BakTest c = new BakTest(new List<string>() { "BTCUSDT", "ETHUSDT" });

            var res = c.getData(timeStamp);

            if(res.timeStamp == timeStamp)
            {
                this.timeStamp++;
            }
            else
            {
                this.timeStamp = res.timeStamp++;
            }

            Console.WriteLine("timeStamp: " + this.timeStamp);
            res.list.ForEach(row =>
            {
                Console.WriteLine("symbol: " + row.symbol);
                Console.WriteLine("price: " + row.price);
                Console.WriteLine("quantity: " + row.quantity);
            });
            Console.WriteLine("------------------------");
        }
    }
}